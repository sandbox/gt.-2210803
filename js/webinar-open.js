jQuery(function ($) {
	if (!webinar) return;
	// www.RTCMultiConnection.org/docs/constructor
	var video = new RTCMultiConnection();
	var screen = new RTCMultiConnection();
	video.direction = 'one-to-many';
	screen.direction = 'one-to-many';

	video.session = {
		video: true,
		audio: true,
		screen: false
	};

	screen.session = {
		video: false,
		audio: false,
		screen: true
	};

	video.mediaConstraints.mandatory = {
		minWidth: 320,
		maxWidth: 640,
		minHeight: 240,
		maxHeight: 480,
		minFrameRate: 10
	};

	screen.mediaConstraints.mandatory = {
		minWidth: 1280,
		maxWidth: 1920,
		minHeight: 1024,
		maxHeight: 1080,
		minFrameRate: 10
	};
	var SIGNALING_SERVER = 'https://'+location.hostname+':8543/';
	video.openSignalingChannel = screen.openSignalingChannel = function (config) {
		var channel = config.channel || this.channel || 'default-namespace';
		var sender = Math.round(Math.random() * 9999999999) + 9999999999;
		io.connect(SIGNALING_SERVER).emit('new-channel', {
			channel: channel,
			sender: sender
		});
		var socket = io.connect(SIGNALING_SERVER + channel);
		socket.channel = channel;
		socket.on('connect', function () {
			if (config.callback) config.callback(socket);
		});
		socket.send = function (message) {
			socket.emit('message', {
				sender: sender,
				data: message
			});
		};
		socket.on('message', config.onmessage);
	};

	video.onstream = function (e) {
		var $element = $(e.mediaElement);
		$element.css({maxWidth: '100%'});
		$('#video-container').empty().append($element);
		$element.get(0).play();
	};
	video.onstreamended = function (e) {
		var $element = $(e.mediaElement);
		$('#video-container').find($element).fadeout(function () {
			$(this).remove();
		});
	};
	screen.onstream = function (e) {
		var $element = $(e.mediaElement);
		$element.css({maxWidth: '100%', width: '100%'});
		$('#screen-container').empty().append($element);
		$element.get(0).play();
	};
	screen.onstreamended = function (e) {
		var $element = $(e.mediaElement);
		$('#screen-container').find($element).fadeout(function () {
			$(this).remove();
		});
	};

	$('#start').click(function () {
		$('#stream-container').fadeIn();
		// www.RTCMultiConnection.org/docs/open
		video.open('video-' + webinar.node_id);
		screen.open('screen-' + webinar.node_id);
		$(this).remove();
	});

});