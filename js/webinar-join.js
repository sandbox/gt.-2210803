jQuery(function ($) {
	if (!webinar) return;

	var video = new RTCMultiConnection();
	var screen = new RTCMultiConnection();

	var SIGNALING_SERVER = 'https://'+location.hostname+':8543/';
	video.openSignalingChannel = screen.openSignalingChannel = function (config) {
		var channel = config.channel || this.channel || 'default-namespace';
		var sender = Math.round(Math.random() * 9999999999) + 9999999999;
		io.connect(SIGNALING_SERVER).emit('new-channel', {
			channel: channel,
			sender: sender
		});
		var socket = io.connect(SIGNALING_SERVER + channel);
		socket.channel = channel;
		socket.on('connect', function () {
			if (config.callback) config.callback(socket);
		});
		socket.send = function (message) {
			socket.emit('message', {
				sender: sender,
				data: message
			});
		};
		socket.on('message', config.onmessage);
	};

	video.onstream = function (e) {
		var $element = $(e.mediaElement);
		$element.css({maxWidth: '100%'});
		$('#video-container').empty().append($element);
		$element.get(0).play();
	};
	video.onstreamended = function (e) {
		var $element = $(e.mediaElement);
		$('#video-container').find($element).fadeout(function () {
			$(this).remove();
		});
	};
	screen.onstream = function (e) {
		var $element = $(e.mediaElement);
		$element.css({maxWidth: '100%', width: '100%'});
		$('#screen-container').empty().append($element);
		$element.get(0).play();
	};
	screen.onstreamended = function (e) {
		var $element = $(e.mediaElement);
		$('#screen-container').find($element).fadeout(function () {
			$(this).remove();
		});
	};

	video.dontAttachStream = true;
	screen.dontAttachStream = true;
	// www.RTCMultiConnection.org/docs/connect
	video.connect('video-'+webinar.node_id);
	screen.connect('screen-'+webinar.node_id);
});