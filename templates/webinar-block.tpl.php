<?php foreach ($nodes as $node): ?>
	<div class="block">
		<h3 class="lined margin-10"><?php print $node->title;?></h3>
		<div class="counter margin-10">
			<?php $btn_class = array('btn', 'small'); ?>
			<?php if ($node->webinar_host > time()): ?>
				<?php $btn_class[] = 'blue'; ?>
				<?php print t('Due').': '.t('@time later', array('@time' => format_interval($node->webinar_open - time()))); ?>
			<?php else: ?>
				<?php $btn_class[] = 'red'; ?>
				<?php print t('Due').': '.t('@time ago', array('@time' => format_interval(time() - $node->webinar_open))); ?>
			<?php endif; ?>
		</div>
		<div>
			<?php print l(t('Join'), 'webinar/'.$node->nid.'/participate', array('attributes' => array('class' => $btn_class))); ?>
			<?php print l(t('Host session'), 'webinar/'.$node->nid.'/host', array('attributes' => array('class' => array('btn', 'small', 'purple')))); ?>
		</div>
	</div>
<?php endforeach; ?>