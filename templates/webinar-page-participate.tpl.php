<?php drupal_add_js(drupal_get_path('module', 'webinar').'/js/webinar-participate.js'); ?>

	<div class="block margin-20">
		<h3><?php print $node->title; ?></h3>

		<div id="stream-container" style="text-align: center;  position: relative;">
			<div id="screen-container">Loading screencast...<br /><a href="https://<?php print $_SERVER['HTTP_HOST']; ?>:8543/socket.io/1/" target="_blank">if not, click here and accept certificate!</a></div>
			<div id="video-container">Loading video stream...<br /><a href="https://<?php print $_SERVER['HTTP_HOST']; ?>:8543/socket.io/1/" target="_blank">if not, click here and accept certificate!</a></div>
		</div>
	</div>



<?php /*if (user_access('host webinar')): ?>
	<h3 class="lined">Webinar control panel</h3>
	<ul>
		<li>People in queue: 0</li>
	</ul>
	<?php print l(t('Host session'), 'webinar/host/'.$node->nid, array('attributes' => array('class' => array('btn', 'small', 'purple')))); ?>
<?php endif;*/ ?>