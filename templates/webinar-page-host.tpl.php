<h3><?php print $node->title; ?></h3>
<?php drupal_add_js(drupal_get_path('module', 'webinar').'/js/webinar-participate.js'); ?>
<div id="stream-container" style="display: none; text-align: center; position: relative;">
	<div id="screen-container">Loading screencast...<br /><a href="https://<?php print $_SERVER['HTTP_HOST']; ?>:8543/socket.io/1/" target="_blank">if not, click here and accept certificate!</a></div>
	<div id="video-container">Loading video stream...<br /><a href="https://<?php print $_SERVER['HTTP_HOST']; ?>:8543/socket.io/1/" target="_blank">if not, click here and accept certificate!</a></div>
</div>
<div style="text-align: center;"><a href="javascript:void();" id="start" class="btn small green">Start session</a></div>