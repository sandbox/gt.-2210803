<?php
/**
 * Page-Level DocBlock example.
 * This DocBlock precedes another DocBlock and will be parsed as the page-level.
 *
 * @package webinar
 * @category Generic module
 * @subpackage webinar.admin.inc
 * @version 0.1
 * @link http://php.net
 * @author gt @ 3/4/14 9:59 PM with PhpStorm
 * @example see root/modules/xyz.php
 */

function _webinar_global_settings() {
	return (object)array_intersect_key(variable_get('webinar_global_settings', array()), array('enabled' => 0, 'signaler_url' => ''));
}

function _webinar_node_type_settings($node_type) {
	if (!empty($node_type)) {
		$query = db_query("SELECT * FROM {webinar_node_type_settings} WHERE node_type = ':node_type' LIMIT 1", array(':node_type' => $node_type));
		if ($query) {
			$settings = (object)$query->fetchAssoc();
			//convert lists to array
			foreach (array('rid_host', 'rid_participant') as $key) {
				$settings->$key = explode(',', $settings->$key);
			}
		}
	}
	return false;
}

/**
 * Implementation of hook_form_FORM_ID().
 */
function webinar_settings_form() {
	$settings = _webinar_global_settings();
	$form = array('webinar_global_settings' => array(
			'#tree' => true)
	);
	$form['webinar_global_settings']['enabled'] = array(
			'#type' => 'select',
			'#title' => t('Enabled'),
			'#options' => array(0 => t('No'), 1 => t('Yes')),
			'#default_value' => $settings->enabled,
			'#description' => t('The module is enabled or not.'),
	);
	$form['webinar_global_settings']['signaler_url'] = array(
			'#type' => 'textfield',
			'#title' => t('Signaler server url'),
			'#default_value' => $settings->signaler_url,
			'#attributes' => array('placeholder' => 'https://'.drupal_valid_http_host($_SERVER['HTTP_HOST']).':8543'),
	    '#description' => t('Set your own signaler server url here. If you leave empty, the free firebase.io app (chat) will be used. You need prefix with protocol scheme. The https: (http over ssl) and wss: (websockets over ssl) supported.'),
	);
	return system_settings_form($form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function webinar_form_node_type_form_alter(&$form, &$form_state) {
	$node_type = $form['#node_type']->type;
  $settings = _webinar_node_type_settings($node_type);
	$field_instances = field_info_instances();
	$textfields = array();
	foreach ($field_instances['node'][$node_type] as $instance) {
		$field = field_info_field($instance['field_name']);
		if ($field['type'] == 'text_long') $textfields[$instance['field_name']] = $instance['label'];
	}
	$roles_host = user_roles(false, 'host webinar');
	$roles_participate = user_roles(false, 'participate webinar');
  $form['webinar_settings'] = array(
    '#type' => 'fieldset',
    '#tree' => true,
    '#title' => t('Webinar settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
    '#access' => user_access('administer webinar'),
  );

  $form['webinar_settings']['enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Enable'),
    '#description' => t('This is a webinar content type?'),
    '#options' => array(0 => t('No'), 1 => t('Yes')),
    '#default_value' => isset($settings->enabled) ? $settings->enabled : 0,
  );

  $form['webinar_settings']['period_field'] = array(
    '#type' => 'select',
    '#title' => t('Period field'),
    '#description' => t('The "%text_type" type field to store webinar period information.', array('%text_type' => t('Long text'))),
    '#options' => $textfields,
    '#default_value' => NULL,
  );

  $form['webinar_settings']['rid_host'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Host user role'),
    '#description' => t('Users with this role will host this type of webinar sessions as a broadcaster.'),
    '#options' => $roles_host,
    '#default_value' => isset($settings->rid_host) ? $settings->rid_host : null,
  );
  $form['webinar_settings']['rid_participant'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Participant user role'),
    '#description' => t('Users with this role will be allowed to join to this type of webinar sessions as a participant.'),
    '#options' => $roles_participate,
    '#default_value' => isset($settings->rid_participant) ? $settings->rid_participant : null,
  );

  //$form['additional_settings']['#attached']['js'][] = drupal_get_path('module', 'webinar') . '/webinar.js';
  $form['#submit'][] = 'webinar_node_type_form_submit';
}

/**
 * Additional submit function for node_type_form().
 */
function webinar_node_type_form_submit($form, &$form_state) {
  $values = $form_state['values'];
}